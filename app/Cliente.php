<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public function scopeName($query, $name) {

    	if (trim($name) != "") {
    		$query->where('nombre_completo', "LIKE", "%$name%");
    	}
    }
}
