@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
<script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/estilo.css">


@auth
<section>
  <h1 class="text-center" style="font-family: 'Anton', sans-serif;"> Listado de Clientes </h1>
  <br>
  <div class="container">
    <div class="row justify-content-around">
      <div class="col">
        <a class="btn btn-large btn-primary" href="{{ route('altaCliente') }}">Agregar Cliente</a>
      </div>
       <form class="form-inline my-2 my-lg-0" action="{{ route('index') }}" method="GET">
      <input class="form-control mr-sm-2" type="text" name="name" id="name" placeholder="Ingrese nombre y/o apellido del cliente" aria-label="Search" required="">
      <button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
    </form>
    </div>
</div>
<br>
<br>
<div class="container">
 <div class="row justify-content-around">
 	<div class="col">
       <form class="form-inline my-2 my-lg-0" action="{{ route('reporte') }}" method="POST">
       	@csrf
      <input class="form-control mr-sm-2" type="date" name="fecha" id="fecha" required="">
      <button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar </button>
    </form>
    </div>
 </div>
</div>

@if (session('mensaje'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('mensaje') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
  @endif
</section>
</br>
</br>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO</th>
      <th scope="col">Telefono(opcional)</th>
      <th scope="col">Segundo Nombre(opcional)</th>
      <th scope="col">OPERACIONES</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($clientes as $item)
    <tr>
      <th scope="row">{{ $item->nombre }}</th>
      <td><strong>{{ $item->apellido }}</strong></td>
      @if ($item->telefono != "")
        <td><strong>{{ $item->telefono }}</strong></td>
      @endif
      @if ($item->telefono == "")
        <td><strong>___</strong></td>
      @endif
       @if ($item->segundo_nombre != "")
        <td><strong>{{ $item->segundo_nombre }}</strong></td>
      @endif
      @if ($item->segundo_nombre == "")
        <td><strong>___</strong></td>
      @endif
      <td>
        <div class="container form-inline">
    
        <button class="btn btn-primary" type="submit"><a href="{{ route('cortes', $item->id) }}"><div style="color: white"> Ficha </div></a></button>

        <form action="{{ route('borrarcliente') }}" method="POST">
        @method('DELETE')
        @csrf
          <input type="hidden" name="id" id="id" value="{{$item->id}}">
          <button class="btn btn-danger float-right" type="submit" onclick="return confirm('Esta seguro que desea borrar este cliente? Si lo borra se eliminaran todos sus cortes.')" title="Eliminar">
          <i class="fas fa-trash"></i>
          </button>
          </form>
         <form action="{{ route('editarcliente') }}" method="POST">
          @csrf
              <input type="hidden" name="id" value="{{$item->id}}">
              <button style="margin: 10px" class="btn btn-primary" type="submit" title="Modificar">
                <i class="fas fa-edit"></i>
              </button>
            </form>
      </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$clientes->links()}}
@endauth


@endsection