@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('https://use.fontawesome.com/releases/v5.0.8/css/solid.css')}}">
<script src="{{asset('https://use.fontawesome.com/releases/v5.0.7/js/all.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('css/estilo.css')}}">

<section>
  <h1 class="text-center" style="font-family: 'Anton', sans-serif;"><strong> {{ $c->nombre_completo }}</u> </strong></h1> 
  <br>
  <div class="container">
    <div class="row justify-content-around">
      <div class="col">
        <button class="btn btn-primary" type="submit"><a href="{{ route('altaCorte', $id) }}"><div style="color: white">Agregar Corte</div></a></button>
      </div>
     </div>
</div>
<br>
@if (session('mensaje'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('mensaje') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
  @endif
</section>
</br>
</br>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Fecha</th>
      <th scope="col">Descripcion</th>
      <th scope="col">Precio</th>
      <th scope="col">OPERACIONES</th>
    </tr>
  </thead>
  <tbody>
  @foreach($cortes as $item)
    <tr>
      <th scope="row">{{ $item->fecha }}</th>
      <th scope="row">{{ $item->descripcion }}</th>
      <th scope="row">{{ $item->precio }}</th>
      <td>
        <form action="{{ route('eliminar') }}" method="POST">
        @method('DELETE')
        @csrf
          <input type="hidden" name="id" id="id" value="{{$item->id}}">
          <button class="btn btn-danger float-right" type="submit" onclick="return confirm('Estas seguro que desea borrar este corte?')" title="Eliminar">
          <i class="fas fa-trash"></i>
          </button>
          </form>
        </div>
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
{{ $cortes->links() }}
<a href="{{ route('index') }}" class="btn btn-primary">Volver al Listado</a>



@endsection