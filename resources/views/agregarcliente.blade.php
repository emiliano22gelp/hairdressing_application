@extends('layouts.app')

@section('content')

<div class="text-center">
<h1> Registro De Cliente </h1>
</div>
</br>	
<form action="{{ route('newCliente') }}" method="POST">
@csrf
    <div class="form-group">
    <label for="exampleInputPassword1"><strong>Ingrese el nombre del Cliente</strong></label>
    <input type="text" class="form-control" id="nombre" name="nombre" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><strong>Ingrese el apellido del Cliente</strong></label>
    <input type="text" class="form-control" id="apellido" name="apellido" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1"><strong>Ingrese el Telefono del Cliente(opcional)</strong></label>
    <input type="number" class="form-control" id="telefono" name="telefono" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1"><strong>Ingrese el Segundo nombre del Cliente(opcional)</strong></label>
    <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp">
  </div>

  <button type="submit" class="btn btn-primary">Registrar Cliente</button>
  <a href="{{ route('index') }}" class="btn btn-primary">Volver al Listado</a> 
</form>

@endsection