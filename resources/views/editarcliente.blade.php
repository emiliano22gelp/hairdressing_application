@extends('layouts.app')

@section('content')

<div class="text-center">
<h1> <strong>  Modificar Datos Del Cliente </strong> </h1>
</div>
</br>	
<form action="{{ route('editcliente', $cliente->id) }}" method="POST">
@method('PUT')
@csrf
    <div class="form-group">
    <label for="exampleInputPassword1"><strong> Modificar Nombre Del Cliente</strong></label>
    <input type="text" class="form-control" id="nombremod" name="nombremod" value="{{ $cliente->nombre }}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><strong>Modificar el apellido del Cliente</strong></label>
    <input type="text" class="form-control" id="apellidomod" name="apellidomod" value="{{ $cliente->apellido }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1"><strong>Modificar/agregar el Telefono del Cliente</strong></label>
    <input type="number" class="form-control" id="telefonomod" name="telefonomod" value="{{ $cliente->telefono }}">
  </div>
  <button type="submit" class="btn btn-primary">Registrar Modificacion</button>
  <a href="{{ route('index') }}" class="btn btn-primary">Volver al Listado</a> 
</form>

@endsection