@extends('layouts.app')

@section('content')

<div class="text-center">
<h1> Nuevo Corte para {{ $c->nombre_completo }}</h1>
</div>
</br>
<form action="{{ route('newCorte') }}" method="POST">
  @csrf
  <div class="form-group">
    <label for="exampleInputPassword1"><strong>Fecha del Corte: {{ $fecha }}</strong></label>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><strong>Ingrese el precio del Corte</strong></label>
    <input type="number" class="form-control" id="precio" name="precio" required>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1"><strong>Ingrese la descripcion del Corte</strong></label>
    <textarea class="form-control" id="motivo" name="motivo" rows="3" required></textarea>
  </div>
  <input type="hidden" name="id" id="id" value="{{ $id }}">
  <button type="submit" class="btn btn-primary">Registrar Corte</button>
  <a href="{{ route('index') }}" class="btn btn-primary">Volver al Listado</a>
  <br>
  <br>
  <a href="{{ route('cortes', $id) }}" class="btn btn-primary">Volver a la Ficha de {{ $c->nombre_completo }}</a>
</form>

@endsection