@extends('layouts.app')

@section('content')

<div class="text-center">
<h1> <strong>  Modificar Corte Del Cliente:    </strong> </h1>
</div>
</br>	
<form action="{{ route('editcorte', $corte->id) }}" method="POST">
@method('PUT')
@csrf
    <div class="form-group">
    <label for="exampleInputPassword1"><strong> Modificar Fecha Del Corte</strong></label>
    <input type="date" class="form-control" id="fechamod" name="fechamod" value="{{ $corte->fecha }}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><strong>Modificar el precio del Corte</strong></label>
    <input type="text" class="form-control" id="preciomod" name="preciomod" value="{{ $corte->precio }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1"><strong>Modificar la descripcion del Corte</strong></label>
    <input type="text" class="form-control" id="descrimod" name="descrimod" value="{{ $corte->descripcion }}">
  </div>
  <button type="submit" class="btn btn-primary">Registrar Modificacion</button>
  <a href="{{ route('index') }}" class="btn btn-primary">Volver al Listado</a> 
</form>

@endsection