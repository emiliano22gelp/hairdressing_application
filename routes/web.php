<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');

Route::delete('/eliminar', 'PagesController@borrarCorte')->name('eliminar');

Route::delete('/borrarcliente', 'PagesController@borrarCliente')->name('borrarcliente');

Route::get('/cortes/{id}', 'PagesController@cortes')->name('cortes');

Route::get('/cliente', 'PagesController@altaCliente')->name('altaCliente');

Route::post('/cliente', 'PagesController@newCliente')->name('newCliente');

Route::get('/nuevocorte/{id}', 'PagesController@altaCorte')->name('altaCorte');

Route::post('/newCorte', 'PagesController@newCorte')->name('newCorte');

Route::post('/editarcliente', 'PagesController@editarcliente')->name('editarcliente');

Route::post('/editarcorte', 'PagesController@editarcorte')->name('editarcorte');

Route::put('/editcliente/{id}', 'PagesController@cambiarcliente')->name('editcliente');

Route::put('/editcorte/{id}', 'PagesController@cambiarcorte')->name('editcorte');

Route::post('/reporte', 'PagesController@buscarcortes')->name('reporte');

Route::get('/reporte', function () {
	 return redirect()->route('index');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
