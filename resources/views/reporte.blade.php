@extends('layouts.app')

@section('content')

@if ($cantidad == 0)
	<h1>No se registraron cortes para la fecha: "{{ $fecha }}"</h1>
@endif
@if ($cantidad > 0)
  <section>
  <h1 class="text-center" style="font-family: 'Anton', sans-serif;">Cortes registrados en la fecha: "{{ $fecha }}"</h1>
  </section>
  </br>
	</br>
	<table class="table">
  	<thead class="thead-dark">
    	<tr>
      	<th scope="col">Nombre</th>
      	<th scope="col">Apellido</th>
      	<th scope="col">Precio abonado</th>
      	<th scope="col">Operaciones</th>
    	</tr>
  	</thead>
  	<tbody>
  	@foreach ($cortes as $corte)
    <tr>
    	<td><strong>{{ $corte->cliente->nombre }}</strong></td>
    	<td><strong>{{ $corte->cliente->apellido }}</strong></td>
    	<td><strong>{{ $corte->precio }}</strong></td>
    	<td>
    	<div class="container form-inline">
        <button class="btn btn-primary" type="submit"><a href="{{ route('cortes', $corte->cliente_id) }}"><div style="color: white">Ficha</div></a></button>
    	</div>
    	</td>
    <tr>
    @endforeach
    </tbody>
	</table>
	<br>
	<h3><strong>Monto total abonado en la fecha "{{ $fecha }}": ${{ $suma }}</strong></h3>
	<br>
@endif
<a href="{{ route('index') }}" class="btn btn-primary">Volver al Listado</a>
@endsection