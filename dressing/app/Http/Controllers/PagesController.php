<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class PagesController extends Controller
{
    public function __construct() {
		session_start();
    	$this->middleware('auth');
	}

	public function index(Request $request) {
		//dd($request->get('cliente'));
		//$clientes = App\Cliente::paginate(2);
		$clientes = App\Cliente::name($request->get('name'))->orderBy('id')->paginate(2);
		return view('welcome', compact('clientes'));
	}

	public function cortes($id) {
		$c = App\Cliente::findOrFail($id);
		$cortes = App\Corte::where('cliente_id', $id)->paginate(10);
		return view('cortes', compact('id', 'cortes', 'c'));
	}

	public function detalleCorte(Request $request) {
		$id = $request->id;
		$corte = App\Corte::findOrFail($id);
		return view('detallecorte', compact('corte'));
	}

	public function altaCliente() {
		return view('agregarcliente');
	}

	public function altaCorte($id) {
		$c = App\Cliente::findOrFail($id);
		$cantidad = App\Corte::where('cliente_id', $id)->count();
		$fecha = date("d-m-Y");
		return view('agregarcorte', compact('id', 'cantidad', 'fecha', 'c'));
	}

	public function newCliente(Request $request) {
		if (($request->telefono != "") and ($request->name != "")) {
			$nombreCompleto = ''.$request->nombre.' '.$request->apellido.'';
			$cliente = new App\Cliente;
			$cliente->nombre = $request->nombre;
			$cliente->apellido = $request->apellido;
			$cliente->nombre_completo = $nombreCompleto;
			$cliente->telefono = $request->telefono;
			$cliente->segundo_nombre = $request->name;
			$cliente->save();
		}
		if (($request->telefono != "") and ($request->name == "")) {
			$nombreCompleto = ''.$request->nombre.' '.$request->apellido.'';
			$cliente = new App\Cliente;
			$cliente->nombre = $request->nombre;
			$cliente->apellido = $request->apellido;
			$cliente->nombre_completo = $nombreCompleto;
			$cliente->telefono = $request->telefono;
			$cliente->save();
		}
		if (($request->telefono == "") and ($request->name != "")) {
			$nombreCompleto = ''.$request->nombre.' '.$request->apellido.'';
			$cliente = new App\Cliente;
			$cliente->nombre = $request->nombre;
			$cliente->apellido = $request->apellido;
			$cliente->nombre_completo = $nombreCompleto;
			$cliente->segundo_nombre = $request->name;
			$cliente->save();
		}
		if (($request->telefono == "") and ($request->name == "")) {
			$nombreCompleto = ''.$request->nombre.' '.$request->apellido.'';
			$cliente = new App\Cliente;
			$cliente->nombre = $request->nombre;
			$cliente->apellido = $request->apellido;
			$cliente->nombre_completo = $nombreCompleto;
			$cliente->save();
		}
		$id = App\Cliente::select('id')->max('id');
		return redirect()->route('cortes', ['id' => $id])->with('mensaje', 'El cliente se ha registrado correctamente!');
	}

	public function newCorte(Request $request) {
		$fecha = date("d-m-Y");
		$corte = new App\Corte;
		$corte->fecha = $fecha;
		$corte->precio = $request->precio;
		$corte->descripcion = $request->motivo;
		$corte->cliente_id = $request->id;
		$corte->save();
		return redirect()->route('cortes', ['id' => $request->id])->with('mensaje', 'El corte se ha registrado correctamente!');
	}

	public function borrarCorte(Request $request) {
		$corte = App\Corte::findOrFail($request->id);
   		$corte->delete();
   		return redirect()->route('cortes', ['id' => $request->id])->with('mensaje', 'El corte se ha eliminado correctamente!');
	}

	public function borrarCliente(Request $request) {
		$cortes = App\Corte::where('cliente_id', $request->id)->delete();
		$cliente = App\Cliente::findOrFail($request->id);
   		$cliente->delete();
   		return redirect('/')->with('mensaje', 'El cliente y todos sus cortes fueron eliminados');
	}

}
